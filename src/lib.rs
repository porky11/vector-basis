#![deny(missing_docs)]
/*!
This crate contains a trait to access the basis vectors of a vector.
It's recommended not to use this crate and instead stick with the vector space crate.
Only use it, when it's really necessary to access the vector components, for example when communicating with a render engine.
**/

use num_traits::One;
use vector_space::VectorSpace;

/// This trait makes it possible to deal with the bases of a vector in a generic way.
pub trait Basis<const I: usize>: VectorSpace {
    /// Creates an unit basis of the vector space.
    fn unit_basis() -> Self {
        Self::basis_of(<Self as VectorSpace>::Scalar::one())
    }
    /// Creates a scaled basis of the vector space of the specified magnitude.
    fn basis_of(magnitude: Self::Scalar) -> Self {
        Self::unit_basis() * magnitude
    }

    /// Queries a basis of a vector space.
    fn basis(&self) -> Self::Scalar;
    /// Queries a basis of a vector space as a mutable reference.
    fn basis_mut(&mut self) -> &mut Self::Scalar;

    /// Creates a new vector with a single basis set to a different value.
    fn with_basis(mut self, magnitude: Self::Scalar) -> Self {
        *self.basis_mut() = magnitude;
        self
    }
}

/// This trait makes it easy to deal with specified bases of a vector in a generic way.
pub trait Bases: VectorSpace {
    #[inline]
    /// Creates the specified unit basis of the vector space.
    fn unit_bases<const I: usize>() -> Self
    where
        Self: Basis<I>,
    {
        Basis::unit_basis()
    }
    #[inline]
    /// Creates the specified basis of the vector of the specified magnitude.
    fn bases_of<const I: usize>(magnitude: Self::Scalar) -> Self
    where
        Self: Basis<I>,
    {
        Basis::basis_of(magnitude)
    }

    #[inline]
    /// Queries the specified basis of a vector space.
    fn bases<const I: usize>(&self) -> Self::Scalar
    where
        Self: Basis<I>,
    {
        Basis::basis(self)
    }
    #[inline]
    /// Queries the specified basis of a vector space as a mutable reference.
    fn bases_mut<const I: usize>(&mut self) -> &mut Self::Scalar
    where
        Self: Basis<I>,
    {
        Basis::basis_mut(self)
    }

    #[inline]
    /// Creates a new vector with the specified basis set to a different value.
    fn with_bases<const I: usize>(self, magnitude: Self::Scalar) -> Self
    where
        Self: Basis<I>,
    {
        Basis::with_basis(self, magnitude)
    }
}

impl<T: VectorSpace> Bases for T {}
